interface Message {
  name: string;
  message: string;
}

function htmlEntities(str: string): string {
  return String(str).replace(/&/g, '&amp;')
    .replace(/</g, '&lt;')
    .replace(/>/g, '&gt;')
    .replace(/"/g, '&quot;');
}

export class UserMessage implements Message {
  public data: { name: string; message: string; type: string };

  constructor(payload: string) {
    try {
      const DATA = JSON.parse(payload);

      if (!DATA.name || !DATA.message || DATA.type !== 'message') {
        throw new Error(`Invalid message payload received: ${payload}`);
      }

      if (DATA.message) {
        DATA.message = htmlEntities(DATA.message);
      }

      this.data = DATA;
    } catch (exp) {
      throw new Error(`Invalid data: ${exp}`);
    }
  }

  get name(): string {
    return this.data.name;
  }

  get message(): string {
    return this.data.message;
  }
}
