import express = require('express');
import morgan = require('morgan');
import bodyParser = require('body-parser');
import path = require('path');

class App {
  public express: any;

  constructor() {
    this.express = express();
    this.mountRoutes();
  }

  private mountRoutes(): void {
    const STATIC_PATH = path.join(__dirname, './../dist');
    this.express.use(express.static(STATIC_PATH));
    this.express.use(morgan('dev'));
    const extended: string = 'true';
    const OPTIONS: any = {
      extended,
    };
    this.express.use(bodyParser.urlencoded(OPTIONS));
    this.express.use(bodyParser.json());
  }
}

export default new App().express;
