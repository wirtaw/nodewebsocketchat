/* eslint-disable */
/// <reference path='../declarations/node.d.ts' />
/// <reference path='../declarations/ws.d.ts' />
/// <reference path='../declarations/rndcolor.d.ts' />
/// <reference path='../declarations/uuid.d.ts' />
/* eslint-enable */
import WebSocket = require('ws');
import models = require('./models');
import randc = require('rand-color-hexadecimal');
import uuid = require('uuid');

class WS {
  public WebSocket:any;

  public server:any;

  public port:number;

  public clients:any[];

  public history:any[];

  public colors:any;

  public serverWS:any;

  constructor() {
    this.server = WebSocket.Server;
    this.port = process.env.PORT || 3000;
    this.clients = [];
    this.history = [];
    this.colors = new Map();
    this.serverWS = null;
    this.init();
  }

  private init(): void {
    const vm:any = this;
    for (let i = 0; i < 100; i += 1) {
      let color = randc();
      if (!this.colors.has(color)) {
        this.colors.set(i, color);
      } else {
        while (this.colors.has(color)) {
          color = randc();
          this.colors.set(i, color);
        }
      }
    }
    this.serverWS = new this.server({ port: this.port });
    this.serverWS.on('connection', (ws: any, req: any) => {
      const ip:string = req.connection.remoteAddress;
      console.dir(ip);
      // console.log(`${(new Date())} Connection from origin ${ws.origin}.`);
      // let connection = ws.accept(null, ws.origin);
      this.clients.push(ws);
      // let index = this.clients.push(ws).length - 1;
      // let userName = false;
      // let userColor = false;


      function broadcast(data: string): void {
        vm.serverWS.clients.forEach((client :any) => {
          client.send(data);
        });
      }

      broadcast(JSON.stringify({ type: 'counter', data: this.clients.length }));
      broadcast(JSON.stringify({ type: 'color', data: this.colors.get(this.clients.length + 1) }));
      if (this.history.length > 0) {
        broadcast(JSON.stringify({ type: 'history', data: this.history }));
      }

      ws.on('message', (message: any) => {
        try {
          const USER_MESSAGE: models.UserMessage = new models.UserMessage(message);
          broadcast(JSON.stringify({ type: 'message', data: USER_MESSAGE.data }));
        } catch (e) {
          console.error(e.message);
        }
      });
    });

    this.serverWS.on('close', (error: any) => {
      function broadcast(data: string): void {
        vm.serverWS.clients.forEach((client :any) => {
          client.send(data);
        });
      }
      console.dir(this.clients);
      this.clients = this.clients.slice(1, this.clients.length);
      broadcast(JSON.stringify({ type: 'counter', data: this.clients.length }));
    });

    console.log('WebSocket server is running on port', this.port);
  }
}

export default new WS();
