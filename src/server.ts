/* eslint-disable */
/// <reference path='../declarations/node.d.ts' />
/// <reference path='../declarations/ws.d.ts' />
/* eslint-enable */
import app from './App';
import ws from './WS';

import debug = require('debug');
import http = require('http');

/**
 * Normalize a port into a number, string, or false.
 */

function normalizePort(val: any) {
  const PORT = parseInt(val, 10);
  if (isNaN(PORT)) {
    // named pipe
    return val;
  }
  if (PORT >= 0) {
    // port number
    return PORT;
  }
  return false;
}

const PORT_APP: number = normalizePort(8777);

/**
 * Event listener for HTTP server "error" event.
 */

function onError(error: any) {
  if (error.syscall !== 'listen') {
    throw error;
  }
  const BIND = `Port ${PORT_APP}`;
  // handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      console.error(`${BIND} requires elevated privileges`);
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error(`${BIND} is already in use`);
      process.exit(1);
      break;
    default:
      throw error;
  }
}

app.set('port', PORT_APP);
const SERVER = http.createServer(app);
SERVER.listen(PORT_APP, (err: any) => {
  if (err) {
    return console.log(err);
  }

  return console.log(`server is listening on ${PORT_APP}`);
});
/**
 * Event listener for HTTP server "listening" event.
 */

function onListening() {
  const ADDR:any = SERVER.address();
  const BIND = typeof ADDR === 'string'
    ? `pipe ${ADDR}`
    : `port ${ADDR.port}`;
  debug(`Listening on ${BIND}`);
}

SERVER.on('error', onError);
SERVER.on('listening', onListening);

const WS = ws;
debugger
