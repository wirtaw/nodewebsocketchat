/* eslint-disable */
/// <reference path='../declarations/node.d.ts' />
/* eslint-enable */
const WS = require('./../src/WS');

describe('WS', (): void => {
  it('clients length should return 0 if no items are passed in', (): void => {
    console.dir(WS);
    expect(WS.clients.length).toBe(0);
  });
});
