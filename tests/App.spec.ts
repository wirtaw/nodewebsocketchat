
import supertest = require('supertest');
import app from '../src/App'

describe('App', () => {
  it('works', async () => {
      const response = await supertest(app).get('/');
      expect(response.status).toBe(200);
      expect(response.header['content-type']).toBe('text/html; charset=UTF-8');
    }
  )
});
